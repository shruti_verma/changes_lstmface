#!/usr/bin/env python

# This is a face recognition on live video from your webcam.
# It includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.

import sys
import os
import argparse
import coloredlogs, logging
coloredlogs.install(level=os.environ.get('LOGGING', logging.DEBUG))

MQTT_PORT = os.environ.get('MQTT_PORT', 1883)

parser = argparse.ArgumentParser(description='Checkout face recognition app')
parser.add_argument('-w', '--web', default=None, action='store_true',
                    help='stream video on web (default: None)')
parser.add_argument('-m', '--module', required=True,
                    help='module to load: checkin|checkout|shelf|object')
parser.add_argument('-v', '--video', type=int, nargs='+', required=True,
                    help='list of video devices')
args = parser.parse_args()

try:
    from process_face import FaceRecognition
    faceRecognition = FaceRecognition(args)

    from flask import Flask, render_template, Response
    app = Flask(args.module)

    @app.route('/')
    def index():
        """Video streaming home page."""
        return render_template('index.html')

    @app.route('/video_feed')
    def video_feed():
        """Video streaming route. Put this in the src attribute of an img tag."""
        return Response(faceRecognition.process_face_video(),
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    app.run(host='0.0.0.0', port=os.environ.get('PORT', 8000), threaded=True)

except KeyboardInterrupt:
    print('Stop application')
