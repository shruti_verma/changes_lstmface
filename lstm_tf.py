#!/usr/bin/env python3

import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.contrib import rnn
import argparse
import cv2
import numpy as np
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
import embeddings
import helper
from lstm_webcam import getRep, rect_to_bb


train_path = "/home/lua/Desktop/openface/aligned_images"
summaries_dir = "/home/lua/Desktop/openface/output"
features, labels = embeddings.unpkl_embeddings("../generated-embeddings/")
print (features.shape, labels.shape)
num_inputs = 1  # number of row that will be given as input
num_seq = features.shape[1]  #  the number of inputs of feature vector we have that is 128 dimesnion vector
classes = helper.get_classes(train_path)
print (classes)
num_classes = len(classes)

# network parameters
learning_rate = 0.001
training_steps = 5000
batch_size = features.shape[0]
num_hidden = 128
display_step = 1000

# splitting dataset into train and validation
X_train, X_val, y_train, y_val = train_test_split(features, labels, test_size=0.20, random_state=42)

with tf.name_scope('input_placeholders') as scope:
    # tf Graph input
    X = tf.placeholder("float", [None, num_seq, num_inputs])
    Y = tf.placeholder("float", [None, num_classes])

with tf.name_scope('weights_biases') as scope:
    # weights and biases
    weights = {
            'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))
            }
    biases = {
            'out': tf.Variable(tf.random_normal([num_classes]))
            }

def RNN(x, weights, biases):
    """each row will be taken one at time"""

    # unstack to get a list of 'timesteps'
    x = tf.unstack(x, num_seq, 1)
    #print  ("i am here, unstacking each row at a time--", x, type (x))
    
     # lstm cell initally has zero state. Defining the basic lstm cell
    lstm_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0, state_is_tuple=True)
    
    # get lstm's current state and hidden state 
    outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)
    
    # getting the last output from the cell
    return tf.matmul(outputs[-1], weights['out']) + biases['out']
    

with tf.name_scope('prediction') as scope:
    logits = RNN (X, weights, biases)
    prediction = tf.nn.softmax(logits)

with tf.name_scope('loss') as scope:
    # loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
            logits=logits, labels=Y))

with tf.name_scope('optimization') as scope:
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op)

with tf.name_scope('model_accuracy') as scope:
    # evalute model accuracy
    correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    


# create a summary for our cost and accuracy
tf.summary.scalar("cost", loss_op)
tf.summary.scalar("accuracy", accuracy)

saver = tf.train.Saver()
# initialize the variables 
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    # merge all summaries into one operation
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter(summaries_dir + '/train', sess.graph)
    
    # summary graph to be saved to output folder
   
    for step in range(1, training_steps+1):
        batch_x, batch_y = X_train, y_train
        batch_x = batch_x.reshape((len(X_train), num_seq, num_inputs))
        sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
        if step % display_step == 0 or step == 1:
            # batch loss and accuracy
            loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                 Y: batch_y})
            print("step " + str(step) + ", batch Loss= " + \
                  "{:.2f}".format(loss) + ", accuracy= " + \
                  "{:.2f}".format(acc))

            summary = sess.run (merged, feed_dict={X: batch_x, Y: batch_y})
            writer.add_summary(summary, step)
    print("training is complete")

    # saving the checkpoint and meta file
    checkpoint_path = saver.save(sess, summaries_dir + "/model.ckpt")
    print("model saved in file: %s" %  checkpoint_path)

    writer.close()

    # find predictions on val set
    val_data = X_val.reshape((len(X_val), num_seq, num_inputs))
    val_label = y_val
    print("Validation Accuracy:", sess.run(accuracy, feed_dict={X: val_data, Y: val_label}))

    predict = tf.argmax(prediction, 1)
    capture = cv2.VideoCapture(0)
    while True:
        ret, frame = capture.read()
        frame = cv2.resize(frame, (640, 480))
        reps, boxes = getRep(frame)
        for (i, rect) in enumerate(boxes):
        	embedding = np.array(reps[i]).reshape(-1, num_seq, num_inputs)
        	cls_pred = predict.eval({X: embedding})
        	person_name = embeddings.get_name(cls_pred)
        	(x, y, w, h) = rect_to_bb(boxes[i])
        	cv2.rectangle(frame, (x,y), (x+w, y+h), (0, 255, 0), 1)
        	cv2.putText(frame, "Pred {}".format(person_name), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
        cv2.imshow("frame", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
        	break

    capture.release()
    cv2.destroyAllWindows()

