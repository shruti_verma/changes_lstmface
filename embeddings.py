#!/usr/bin/env python2

import os
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
from operator import itemgetter
import argparse
import pickle


# saves the embeddings in pickle file to be work as input to lstm layers
def pkl_embeddings():
    print("Loading embeddings.")
    fname = "{}/labels.csv".format(args.workDir)
    labels = pd.read_csv(fname, header=None).as_matrix()[:, 1]
    labels = list(map(itemgetter(1), map(os.path.split, map(os.path.dirname, labels))))
    fname = "{}/reps.csv".format(args.workDir)
    embeddings = pd.read_csv(fname, header=None).as_matrix()
    le = LabelEncoder().fit(labels)
    labelsNum = le.transform(labels)
    print (le.inverse_transform([2]))
    labelsNum = labelsNum.reshape(-1, 1)

    # one hot encoding of lables
    hotenc = OneHotEncoder()
    hotenc.fit(labelsNum)
    new_labels = hotenc.transform(labelsNum).toarray()
    with open(args.workDir + "embeddings.pkl", "wb") as fout:
        pickle.dump((embeddings, new_labels), fout)
        
# get embeddings from the pickle file
def unpkl_embeddings(dir_path):
    with open(dir_path + "embeddings.pkl", "rb") as fin:
        (features, labels) = pickle.load(fin)
    
    return (features, labels)

def get_name(pred_id):
    fname = "{}/labels.csv".format("../generated-embeddings")
    labels = pd.read_csv(fname, header=None).as_matrix()[:, 1]
    labels = list(map(itemgetter(1), map(os.path.split, map(os.path.dirname, labels))))
    le = LabelEncoder().fit(labels)
    return (le.inverse_transform(pred_id))

    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--emb_dir', type=str, dest='workDir', help="Path to the generated embedding csv files.")
    
    args = parser.parse_args()
    
    pkl_embeddings()
    
