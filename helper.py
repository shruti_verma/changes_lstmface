""" Data setup and methods to be used in LSTM tensorflow script """

import os
import glob
import numpy as np
import random
import cv2
from sklearn.utils import shuffle
import matplotlib.pyplot as plt


class DataSet(object):
    
  def __init__(self, images, labels, ids, cls):
   # print ("yes i am called")
    self._num_examples = images.shape[0]


    self._images = images
    self._labels = labels
    self._ids = ids
    self._cls = cls
    self._epochs_completed = 0
    self._index_in_epoch = 0

  @property
  def images(self):
    return self._images

  @property
  def labels(self):
    return self._labels

  @property
  def ids(self):
    return self._ids

  @property
  def cls(self):
    return self._cls

  @property
  def num_examples(self):
    return self._num_examples

  @property
  def epochs_completed(self):
    return self._epochs_completed

  def next_batch(self, batch_size):
    """ Return the next `batch_size` examples from this data set """
    start = self._index_in_epoch
    self._index_in_epoch += batch_size

    if self._index_in_epoch > self._num_examples:
      # Finished epoch
      self._epochs_completed += 1
      # Start next epoch
      start = 0
      self._index_in_epoch = batch_size
      assert batch_size <= self._num_examples
    end = self._index_in_epoch

    return self._images[start:end], self._labels[start:end], self._ids[start:end], self._cls[start:end]


def read_data(path, classes):
    class Datasets (object):
        pass
    
    images = []
    labels = []
    ids = []
    cls = []
    
    # data directory has a separate folder for each class and further each with name of class
    for i in classes:
        index = classes.index(i)
        subdir_path = os.path.join(path, i, '*g')
        files = glob.glob(subdir_path)
        for fp in files:
            read_img = cv2.imread(fp)
            images.append(read_img)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            flbase = os.path.basename(fp)
            ids.append(flbase)
            cls.append(i)
        
    images = np.array(images)
    labels = np.array(labels)
    ids = np.array(ids)
    cls = np.array(cls)
    
    return images, labels, ids, cls
    
    
def load_data(train_path, classes, validation_size=0.2):
    
    class DataSets(object):
        pass
    data_sets = DataSets()

    images, labels, ids, cls = read_data(train_path, classes)
    images, labels, ids, cls = shuffle(images, labels, ids, cls)  # shuffle the data

    if isinstance(validation_size, float):
        validation_size = int(validation_size * images.shape[0])

    validation_images = images[:validation_size]
    validation_labels = labels[:validation_size]
    validation_ids = ids[:validation_size]
    validation_cls = cls[:validation_size]

    train_images = images[validation_size:]
    train_labels = labels[validation_size:]
    train_ids = ids[validation_size:]
    train_cls = cls[validation_size:]

    data_sets.train = DataSet(train_images, train_labels, train_ids, train_cls)
    data_sets.valid = DataSet(validation_images, validation_labels, validation_ids, validation_cls)

    return data_sets


def load_test_data(test_path, classes):
    print ("called")
    test_img, test_labels, test_ids, test_cls = read_data (test_path, classes)
    return test_img, test_labels, test_ids, test_cls


def plot_images(images, img_size, num_channels, cls_true, cls_pred=None):
    if len(images) == 0:
        print("no images to show")
        return 
    else:
        random_indices = random.sample(range(len(images)), min(len(images), 9))

    images, cls_true  = zip(*[(images[i], cls_true[i]) for i in random_indices])
    
    # figure with 3x3 sub-plots.
    fig, axes = plt.subplots(3, 3)
    fig.subplots_adjust(hspace=0.3, wspace=0.3)

    for i, ax in enumerate(axes.flat):
        # plot image
        ax.imshow(images[i].reshape(img_size, img_size, num_channels))

        # true and predicted classes.
        if cls_pred is None:
            xlabel = "True: {0}".format(cls_true[i])
        else:
            xlabel = "True: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])

        # classes as the label on the x-axis.
        ax.set_xlabel(xlabel)
        ax.set_xticks([])
        ax.set_yticks([])
    
    return fig

def get_classes(dir_path):
    cls_list = list()
    for root, dirs, files in os.walk(dir_path, topdown=False):
        for name in dirs:
             cls_list.append(name)
    return cls_list
