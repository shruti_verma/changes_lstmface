import cv2
import os
import pickle
import sys

import numpy as np
np.set_printoptions(precision=2)
from sklearn.mixture import GMM
import openface

#fileDir = os.path.dirname(os.path.realpath(__file__))
#modelDir = os.path.join(fileDir, '..', 'models')
dlibModelDir = "/root/openface/models/dlib/shape_predictor_68_face_landmarks.dat"
openfaceModelDir = "/root/openface/models/openface/nn4.small2.v1.t7"

align = openface.AlignDlib(dlibModelDir)
net = openface.TorchNeuralNet(openfaceModelDir, imgDim=96, cuda=True)


def getRep(bgrImg):
    if bgrImg is None:
        raise Exception("Unable to load image/frame")

    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

    print("  + Original size: {}".format(rgbImg.shape))

    # Get the largest face bounding box
    # bb = align.getLargestFaceBoundingBox(rgbImg) #Bounding box

    # Get all bounding boxes
    bb = align.getAllFaceBoundingBoxes(rgbImg)

    if bb is None:
        # raise Exception("Unable to find a face: {}".format(imgPath))
        return None
    else:
        print("Face detection done")

    alignedFaces = []
    boxes = []
    for box in bb:
        alignedFaces.append(align.align(96, rgbImg, box, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE))
        boxes.append(box)

    if alignedFaces is None:
        raise Exception("Unable to align the frame")
    else:
        print ("Face alignment Done")

    reps = []
    for alignedFace in alignedFaces:
        reps.append(net.forward(alignedFace))

    return reps, boxes

def rect_to_bb(rect):
    # take a bounding predicted by dlib and convert it
    # to the format (x, y, w, h) as we would normally do
    # with OpenCV
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    # return a tuple of (x, y, w, h)
    return (x, y, w, h)
