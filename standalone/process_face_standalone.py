import cv2
import numpy as np
import os
import sys
import argparse
import imagehash
import json
from PIL import Image
import urllib
import base64
# from lib import *
import time
import tensorflow as tf
from tensorflow.contrib import rnn
import pickle
from threading import Thread
import collections, itertools


try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

# from sklearn.decomposition import PCA
# from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import OneHotEncoder
# from sklearn.svm import SVC
# from sklearn.manifold import TSNE
import dlib


import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

BUFFER_MEDIAN = 10
# network parameters
training_steps = 5000
display_step = 1000

fileDir = '/root/openface'
sys.path.append(os.path.join(fileDir))
modelDir = os.path.join(fileDir, 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

import openface

imgDim = 96
align = openface.AlignDlib(os.path.join(dlibModelDir, "shape_predictor_68_face_landmarks.dat"))
net = openface.TorchNeuralNet(os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'), imgDim=imgDim, cuda=False)

IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480

# timer = Timer()

class Model(object):

    def __init__(self, summary_dir):
        self.summary_dir = summary_dir
        self.learning_rate = 0.001
        self.num_seq = 128
        self.num_inputs = 1
        self.num_hidden = 128
        self.num_classes = 1

    def train(self, X, y, num_classes):
        self.X = X
        self.y = y
        self.num_classes = num_classes
        # tf Graph input placholders
        X = tf.placeholder("float", [None, self.num_seq, self.num_inputs])
        Y = tf.placeholder("float", [None, self.num_classes])
        # weights and biases
        weights = {'out': tf.Variable(tf.random_normal([self.num_hidden, self.num_classes]))}
        biases = {'out': tf.Variable(tf.random_normal([self.num_classes]))}
        #predictions
        logits = self.lstmcell(X, weights, biases)
        prediction = tf.nn.softmax(logits)
        # loss operation
        loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
                    logits=logits, labels=Y))
        # optimization
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        train_op = optimizer.minimize(loss_op)
        # evalute model accuracy
        correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        saver = tf.train.Saver()

        start_time = time.time()
        with tf.Session() as sess:
            # initialize the variables
            init = tf.global_variables_initializer()
            sess.run(init)
            # file writer path to save the model
            writer = tf.summary.FileWriter(self.summary_dir + '/train', sess.graph)
            # start training
            for step in range(1, training_steps+1):
                batch_x, batch_y = self.X, self.y
                batch_x = batch_x.reshape((len(self.X), self.num_seq, self.num_inputs))
                sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
                if step % display_step == 0 or step == 1:
                    # batch loss and accuracy
                    loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                             Y: batch_y})
                    print("step " + str(step) + ", batch Loss= " + \
                              "{:.2f}".format(loss) + ", accuracy= " + \
                              "{:.2f}".format(acc))

            print("training is complete in {} seconds".format(time.time() - start_time))
            # saving the checkpoint and meta file
            model_path = saver.save(sess, self.summary_dir + "/model.ckpt")
            print("model saved in file: %s" %  model_path)
        writer.close()

    def lstmcell(self, x, weights, biases):
        """each row will be taken one at time"""
        # unstack to get a list of 'timesteps'
        x = tf.unstack(x, self.num_seq, 1)
        #print  ("i am here, unstacking each row at a time--", x, type (x))
        # lstm cell initally has zero state. Defining the basic lstm cell
        lstm_cell = rnn.BasicLSTMCell(self.num_hidden, forget_bias=1.0, state_is_tuple=True)
        # get lstm's current state and hidden state
        outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)
        # getting the last output from the cell
        return tf.matmul(outputs[-1], weights['out']) + biases['out']

    def predict(self, X_data):
        self.X = X_data
        print ("I am inside prediction>>>", len(self.X))
        if os.path.exists("output/model.ckpt.meta"):
            # tf Graph input placholders
            X = tf.placeholder("float", [None, self.num_seq, self.num_inputs])
            # Two Tensor Variables: weights and bias
            weights = {'out': tf.Variable(tf.random_normal([self.num_hidden, self.num_classes]))}
            biases = {'out': tf.Variable(tf.random_normal([self.num_classes]))}
            logits = self.lstmcell(X, weights, biases)
            prediction = tf.nn.softmax(logits)
            predict_prob = prediction
            pred = tf.argmax(prediction, 1)
            saver = tf.train.Saver()
            with tf.Session() as sess:
                # Load the weights and bias
                saver.restore(sess, "output/model.ckpt")
                for i in range(len(self.X)):
                    embedding = np.array(self.X[i]).reshape(-1, self.num_seq, self.num_inputs)
                    cls_pred = pred.eval({X: embedding})
                    confidence = np.amax((predict_prob.eval({X: embedding})), axis=1)
                    print ("cls pred {} confidence {}".format(cls_pred, confidence))
        else:
            print("Could not laod model")
            return None

    def validate(self, test_Data, test_labels):
        pass


class Face:
    def __init__(self, rep, identity):
        self.rep = rep
        self.identity = identity

    def __repr__(self):
        return "{{id: {}, rep[0:5]: {}}}".format(
            str(self.identity),
            self.rep[0:5]
        )

class FaceRecognition(object):
    """FaceRecognition class"""
    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def __init__(self, args):
        super(FaceRecognition, self).__init__()
        self.stop = False
        self.FPS = 2
        self.capture_face = None
        self.training = True
        self.identities = []
        self.people = {}
        self.predictedUsers = []
        self.lengthPredictedUsers = 0
        self.cumulativeMedian = -1
        self.predictions = []
        self.frameTot = 80
        self.nThreads = 2
        self.bufferFrames = collections.deque(maxlen=self.frameTot)
        self.isLockTraining = False

        # init db with 2 generic faces (1, 2)
        self.images = []
        self.buff = []
        self.model = Model(summary_dir="output")
        #filename = 'faces/db.pkl'
        #if os.path.isfile(filename):
            #self.images = pickle.load(open(filename, "rb"))
            #self.trainLSTM()       # call LSTM for training
            #self.numIdentity = 2

        self.cameraIds = args.test_video
        print (self.cameraIds)
        self.cameras = []
        for camId in self.cameraIds:
            capture_face = cv2.VideoCapture(camId)
            capture_face.set(3,1280)
            capture_face.set(4,720)
            # # capture_face = CamVideoStream(camId).start()
            # except:
            #     print ("Problem opening input stream on camera " + str(camId))
            #     continue

            if capture_face.isOpened():
                print ("Capture stream is opened on camera " + str(camId))
                self.cameras.append(capture_face)
                self.capture_face = self.cameras[0]
                print (self.cameras)

        print ('----------------------------------------')
        print ('Video face cameras init:', self.cameras)

    def process_face_video(self):
        count = 0
        cap = cv2.VideoCapture(self.cameraIds[0])
        while cap.isOpened() and count < 20:
            ret, frame = cap.read()
            frame = cv2.resize(frame,(IMAGE_WIDTH,IMAGE_HEIGHT))
            if self.isBlurry(frame):
                pass
            else:
                self.buff.append(frame)
            count += 1
        cap.release()
        self.processFrame(self.buff)

    def getData(self):
        X = []
        y = []
        for im in self.images:
            X.append(im[0])
            y.append(im[1])

        numIdentities = len(set(y + [-1])) - 1
        print (numIdentities)
        if numIdentities == 0:
            return None

        X = np.vstack(X)
        # y has to be reshaped so that it can be one hot encoded as required to feed on lstm layer
        y = np.array(y).reshape((-1, 1))
        hotenc = OneHotEncoder()
        hotenc.fit(y)
        y_labels = hotenc.transform(y).toarray()
        return (X, y_labels, numIdentities)

    def trainLSTM(self):
        print("+ Training LSTM on {} labeled images.".format(len(self.images)))

        d = self.getData()
        if d is None:
            return
        (X, y, num_cls) = d
        print (X.shape, y.shape)
        # self lstm code will run here
        self.model.train(X, y, num_cls)

    def preLSTMtrain(self, frames, numThread):
        cnt = 0
        for frame in frames:
            cnt += 1
            print ("Training on face count {}\n".format(cnt))
            reps, boxes = self.getRep(frame)
            for i in range(len(reps)):
                self.images.append([reps[i], i])
        return self.images

    def isBlurry(self, img, thresh = 100):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        variance = cv2.Laplacian(img, cv2.CV_64F).var()
        if variance < thresh:
            logging.warning('Image is blurry; Laplace variance %s', variance)
            return True

        return False

    def getRep(self, bgrImg):
        if bgrImg is None:
            raise Exception("Unable to load image/frame")

        rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

        # Get all bounding boxes
        bb = align.getAllFaceBoundingBoxes(rgbImg)

        if bb is None:
            # raise Exception("Unable to find a face: {}".format(imgPath))
            return None
        else:
            print("Face detection done")

        alignedFaces = []
        boxes = []
        for box in bb:
            alignedFaces.append(align.align(96, rgbImg, box, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE))
            boxes.append(box)

        if alignedFaces is None:
            raise Exception("Unable to align the frame")
        else:
            print ("Face alignment Done")

        reps = []
        for alignedFace in alignedFaces:
            reps.append(net.forward(alignedFace))
        return reps, boxes

    def processFrame(self, frames):
        embeds = []
        if not frames:
            print ("frames are not captured")
        else:
            for i in range(len(frames)):
                reps, boxes = self.getRep(frames[i])
                embeds.append(reps)
        if self.model.predict(embeds) is None:
            print ("Need to train frames\n.")
            self.images = self.preLSTMtrain(frames, 1)
            self.trainLSTM()
