# Demo Docker Image
#
# Build image
#   nvidia-docker build -t changes_lstmface .
#
# Run with interactive shell
#   nvidia-docker run -v /dev/video0:/dev/video0 -p 8000:8000 -t -i changes_lstmface /bin/bash
#
# Download image
#   nvidia-docker pull server-one:5000/changes_lstmface/app:latest
#
# Tag images from local copy to the docker registry on server-one
#   nvidia-docker tag changes_lstmface:latest server-one:5000/changes_lstmface/app
#
# Commit and push changes
#   docker commit -m "<MESSAGE>" CONTAINER server-one:5000/changes_lstmface/app
#   docker push server-one:5000/changes_lstmface/app:latest
#
# Run container sharing video device `/dev/video0`
#   nvidia-docker run -v /home/igor/Work/checkoutpirates/changes_lstmface:/app --device /dev/video0 -p 3000:3000 -p 8000:8000 -p 9000:9000 -t -i server-one:5000/changes_lstmface/app:latest /bin/bash
#

################################################################################
# TENSORFLOW
# Tensorflow-gpu standard Docker image
#   @see https://github.com/tensorflow/tensorflow/tree/master/tensorflow/tools/docker
FROM gcr.io/tensorflow/tensorflow:latest-gpu
MAINTAINER Igor Moiseev <igor@checkoutfree.it>

RUN apt-get update && apt-get install -y \
    curl \
    git \
    graphicsmagick \
    libssl-dev \
    libffi-dev \
    python-dev \
    wget \
    zip \
    python3-pip \
    sudo \
    build-essential \
    cmake \
    curl \
    gfortran \
    git \
    python-tk \
    python-numpy python-dev python-pip python-setuptools \
    libboost-all-dev libblas-dev liblapack-dev libatlas-base-dev gfortran \
    libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev \
    libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev \
    build-essential \
    unzip \
    yasm \
    pkg-config \
    libpq-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN python3 -m pip install --upgrade --force pip && \
    pip3 install dlib scikit-learn numpy

RUN cd ~ && \
    git clone https://github.com/torch/distro.git ~/torch --recursive && \
    cd ~/torch; bash install-deps && \
    ./install.sh && \
    /bin/bash -c "source ~/torch/install/bin/torch-activate"

################################################################################
# TORCH
RUN cd ~/torch/install/bin && \
    ./luarocks install dpnn && \
    ./luarocks install nn && \
    ./luarocks install optim && \
    ./luarocks install optnet && \
    ./luarocks install csvigo && \
    ./luarocks install cutorch && \
    ./luarocks install cunn && \
    #./luarocks install fblualib && \
    ./luarocks install torchx && \
    ./luarocks install image && \
    ./luarocks install tds

WORKDIR /
RUN wget https://github.com/opencv/opencv/archive/3.3.0.zip \
    && unzip 3.3.0.zip \
    && mkdir /opencv-3.3.0/cmake_binary \
    && cd /opencv-3.3.0/cmake_binary \
    && cmake -DBUILD_TIFF=ON \
      -DBUILD_opencv_java=OFF \
      -DWITH_CUDA=OFF \
      -DENABLE_AVX=ON \
      -DWITH_OPENGL=ON \
      -DWITH_OPENCL=ON \
      -DWITH_IPP=ON \
      -DWITH_TBB=ON \
      -DWITH_EIGEN=ON \
      -DWITH_V4L=ON \
      -DBUILD_TESTS=OFF \
      -DBUILD_PERF_TESTS=OFF \
      -DCMAKE_BUILD_TYPE=RELEASE \
      -DCMAKE_INSTALL_PREFIX=$(python3 -c "import sys; print(sys.prefix)") \
      -DPYTHON_EXECUTABLE=$(which python3) \
      -DPYTHON_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
      -DPYTHON_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") .. \
    && make install \
    && rm /3.3.0.zip \
    && rm -r /opencv-3.3.0

RUN cd ~ && git clone https://github.com/cmusatyalab/openface.git && \
    cd ~/openface && \
    ./models/get-models.sh && \
    pip3 install -r requirements.txt && \
    python3 setup.py install && \
    pip3 install --user --ignore-installed -r demos/web/requirements.txt && \
    pip3 install -r training/requirements.txt

################################################################################

VOLUME ["/app"]
WORKDIR "/app"
