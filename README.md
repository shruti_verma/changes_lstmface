## Pipeline

I have placed all the code files in the demos folder

```
1. ./util/align-dlib.py ./training-images/ align outerEyesAndNose ./aligned-images/ --size 96
2. ./batch-represent/main.lua -outDir ./generated-embeddings/ -data ./aligned-images/
3. run embeddings.py (python embeddings.py --emb_dir path to save embeddings.pkl
4. run lstm_tf.py
```
adding another code for testing after loading the model


## Running the app

Build Docker image

```
nvidia-docker build -t changes_lstmface .
```

Run with camera attached from the app folder `/changes_lstmface (master)$``

```
nvidia-docker run --name changes_lstmface  -v $PWD:/app  --device /dev/video0  -p 8000:8000  -t -i changes_lstmface  /bin/bash
```

once created re-enter with

```
nvidia-docker start -i changes_lstmface
```

Then install dependences

```
pip3 install -r requirements.txt
```

and start the App

```
root@a50cb6d60fd8:/app# python3 app.py --video 0 --module checkin
```

goto http://localhost:8000/
