import cv2
import numpy as np
import os
import sys
import argparse
import imagehash
import json
from PIL import Image
import urllib
import base64
# from lib import *
import time
import tensorflow as tf
from tensorflow.contrib import rnn
import pickle
from threading import Thread
import collections, itertools


try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

# from sklearn.decomposition import PCA
# from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import OneHotEncoder
# from sklearn.svm import SVC
# from sklearn.manifold import TSNE
import dlib


import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

BUFFER_MEDIAN = 10
# network parameters
training_steps = 5000
display_step = 1000

fileDir = '/root/openface'
sys.path.append(os.path.join(fileDir))
modelDir = os.path.join(fileDir, 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

import openface

imgDim = 96
align = openface.AlignDlib(os.path.join(dlibModelDir, "shape_predictor_68_face_landmarks.dat"))
net = openface.TorchNeuralNet(os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'), imgDim=imgDim, cuda=False)

IMAGE_WIDTH = 400
IMAGE_HEIGHT = 300

# timer = Timer()

class Model(object):

    def __init__(self, summary_dir):
        self.summary_dir = summary_dir
        self.learning_rate = 0.001
        self.num_seq = 128
        self.num_inputs = 1
        self.num_hidden = 128
        self.num_classes = 0

    def train(self, X, y, num_classes):
        self.X = X
        self.y = y
        self.num_classes = num_classes
        # tf Graph input placholders
        X = tf.placeholder("float", [None, self.num_seq, self.num_inputs])
        Y = tf.placeholder("float", [None, self.num_classes])
        # weights and biases
        weights = {'out': tf.Variable(tf.random_normal([self.num_hidden, self.num_classes]))}
        biases = {'out': tf.Variable(tf.random_normal([self.num_classes]))}
        #predictions
        logits = self.lstmcell(X, weights, biases)
        prediction = tf.nn.softmax(logits)
        # loss operation
        loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
                    logits=logits, labels=Y))
        # optimization
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        train_op = optimizer.minimize(loss_op)
        # evalute model accuracy
        correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        saver = tf.train.Saver()

        start_time = time.time()
        with tf.Session() as sess:
            # initialize the variables
            init = tf.global_variables_initializer()
            sess.run(init)
            # file writer path to save the model
            writer = tf.summary.FileWriter(self.summary_dir + '/train', sess.graph)
            # start training
            for step in range(1, training_steps+1):
                batch_x, batch_y = self.X, self.y
                batch_x = batch_x.reshape((len(self.X), self.num_seq, self.num_inputs))
                sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
                if step % display_step == 0 or step == 1:
                    # batch loss and accuracy
                    loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                             Y: batch_y})
                    print("step " + str(step) + ", batch Loss= " + \
                              "{:.2f}".format(loss) + ", accuracy= " + \
                              "{:.2f}".format(acc))

            print("training is complete in {} seconds".format(time.time() - start_time))
            # saving the checkpoint and meta file
            checkpoint_path = saver.save(sess, self.summary_dir + "/model.ckpt")
            print("model saved in file: %s" %  checkpoint_path)
        writer.close()

    def lstmcell(self, x, weights, biases):
        """each row will be taken one at time"""
        # unstack to get a list of 'timesteps'
        x = tf.unstack(x, self.num_seq, 1)
        #print  ("i am here, unstacking each row at a time--", x, type (x))
        # lstm cell initally has zero state. Defining the basic lstm cell
        lstm_cell = rnn.BasicLSTMCell(self.num_hidden, forget_bias=1.0, state_is_tuple=True)
        # get lstm's current state and hidden state
        outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)
        # getting the last output from the cell
        return tf.matmul(outputs[-1], weights['out']) + biases['out']

    def predict(self, X_data):
        self.X = X_data
        print ("I am inside prediction>>>", self.X.shape)
        pretrained_modelpath = self.summary_dir + "/model.ckpt"
        if os.path.exists(pretrained_modelpath):
            # tf Graph input placholders
            X = tf.placeholder("float", [None, self.num_seq, self.num_inputs])
            # Two Tensor Variables: weights and bias
            weights = {'out': tf.Variable(tf.random_normal([self.num_hidden, self.num_classes]))}
            biases = {'out': tf.Variable(tf.random_normal([self.num_classes]))}
            logits = self.lstmcell(X, weights, biases)
            prediction = tf.nn.softmax(logits)
            predict_prob = prediction
            pred = tf.argmax(prediction, 1)
            saver = tf.train.Saver()
            with tf.Session() as sess:
                # Load the weights and bias
                saver.restore(sess, pretrained_modelpath)
                embedding = np.array(self.X).reshape(-1, self.num_seq, self.num_inputs)
                cls_pred = pred.eval({X: embedding})
                confidence = np.amax((predict_prob.eval({X: embedding})), axis=1)

            return (cls_pred, confidence)

        else:
            print ("Could not load pre trained model.")
            return None


    def validate(self, test_Data, test_labels):
        pass


class Face:
    def __init__(self, rep, identity):
        self.rep = rep
        self.identity = identity

    def __repr__(self):
        return "{{id: {}, rep[0:5]: {}}}".format(
            str(self.identity),
            self.rep[0:5]
        )

class FaceRecognition(object):
    """FaceRecognition class"""
    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def __init__(self, args):
        super(FaceRecognition, self).__init__()
        self.stop = False
        self.FPS = 2
        self.capture_face = None
        self.training = True
        self.identities = []
        self.people = {}
        self.predictedUsers = []
        self.lengthPredictedUsers = 0
        self.cumulativeMedian = -1
        self.predictions = []
        self.frameTot = 20
        self.nThreads = 2
        self.bufferFrames = collections.deque(maxlen=self.frameTot)
        self.isLockTraining = False

        # init db with 2 generic faces (1, 2)
        self.images = {}
        self.numIdentity = 0
        self.model = Model(summary_dir="output")
        # filename = 'faces/db.pkl'
        # if os.path.isfile(filename):
        #     self.images = pickle.load(open(filename, "rb"))
        #     self.trainLSTM()       # call LSTM for training
        #     self.numIdentity = 2

        self.cameraIds = args.video
        self.cameras = []
        for camId in self.cameraIds:
            capture_face = cv2.VideoCapture(camId)
            capture_face.set(3,1280)
            capture_face.set(4,720)
            # # capture_face = CamVideoStream(camId).start()
            # except:
            #     print ("Problem opening input stream on camera " + str(camId))
            #     continue

            if capture_face.isOpened():
                print ("Capture stream is opened on camera " + str(camId))
                self.cameras.append(capture_face)
                self.capture_face = self.cameras[0]

        print ('----------------------------------------')
        print ('Video face cameras init:', self.cameras)

    def process_face_video(self):
        cnt = 0
        while True:
            cnt += 1
            if cnt % self.FPS == 0:
                camId = 0
                self.predictions = []
                for camera in self.cameras:
                    ret, img = camera.read()
                    img_buf = img
                    ##ret, img =
                    self.processFrame(img, camId)
                    print ("process frame has been called here.\n")
                    #camId += 1

                ##if ret and img is not None:
                    #pass
                #     if self.lengthPredictedUsers>0 and self.predictedUsers[-1] == 1:
                #         img_buf = cv2.resize(img_buf,(IMAGE_WIDTH,IMAGE_HEIGHT))
                #         self.bufferFrames.append(img_buf)

                    ##yield (b'--frame\r\n'
                           ##b'Content-Type: image/jpeg\r\n\r\n' + img + b'\r\n\r\n')
            # else:
            #     if self.lengthPredictedUsers>0 and self.predictedUsers[-1] == 1:
            #         for camera in self.cameras:
            #             ret, img = camera.read()
            #             if ret and img is not None:
            #                 img = cv2.resize(img,(IMAGE_WIDTH,IMAGE_HEIGHT))
            #                 self.bufferFrames.append(img)

    def crop(self, img, bb):
        if(
            img is not None and
            bb.top()>0 and bb.bottom()>0 and bb.bottom() - bb.top() > 10 and
            bb.left()>0 and bb.right()>0 and bb.right() - bb.left() > 10
        ):
            bbFixed = {
                'top': bb.center().y - 100,
                'bottom': bb.center().y + 100,
                'left': bb.center().x - 80,
                'right': bb.center().x + 80,
            }
            img = img[bbFixed['top']:bbFixed['bottom'], bbFixed['left']:bbFixed['right'], :]
            if len(img)>0:
                return img

        return None

    def getData(self):
        X = []
        y = []
        for img in self.images.values():
            X.append(img.rep)
            y.append(img.identity)

        numIdentities = len(set(y + [-1])) - 1
        if numIdentities == 0:
            return None

        X = np.vstack(X)
        # y has to be reshaped so that it can be one hot encoded as required to feed on lstm layer
        y = np.array(y).reshape((-1, 1))
        hotenc = OneHotEncoder()
        hotenc.fit(y)
        y_labels = hotenc.transform(y).toarray()
        return (X, y_labels)

    def trainLSTM(self):
        print("+ Training LSTM on {} labeled images.".format(len(self.images)))

        d = self.getData()
        if d is None:
            return

        (X, y) = d
        print ("X {} y {}".format(X, y))
        print(np.size(X), np.size(y))
        numIdentities = len(y)
        # if numIdentities <= 1:
        #     return

        # self lstm code will run here
        self.model.train(X, np.transpose(y), numIdentities)

    def isUnknowFaceISKnown(self, alignedFace):
        rep = net.forward(alignedFace).reshape(1, -1)
        pred_cls_id, condfidence = mode.predict(rep)

        if confidence > 0.8:
            predictedIdentity = pred_cls_id
            if predictedIdentity not in (1, 2):
                return 0

        return 1

    def trainUnknownFace(self):
        frames = []
        self.isLockTraining = True
        self.FPS = 10
        self.numIdentity += 1
        cnt = 0
        checkSum = 0
        images = {}

        print ("Start Training on new person")
        # self.socketio.emit('userTraining', self.numIdentity, namespace='/api')

        # while len(self.bufferFrames) < self.frameTot and cnt < self.frameTot/2:
        #     for camera in self.cameras:
        #         ret, frame = camera.read()
        #         if ret and frame is not None:
        #             cnt += 1
        #             frame = cv2.resize(frame,(IMAGE_WIDTH,IMAGE_HEIGHT))
        #             self.bufferFrames.append(frame)
        #
        # print ('length self.bufferFrames: ', len(self.bufferFrames))
        # # self.socketio.emit('setUser', self.numIdentity, namespace='/api')
        #
        # threads = []
        # for i in range(self.nThreads):
        #     # sliceBufferFrames = list(itertools.islice(self.bufferFrames, i*self.frameTot/self.nThreads, (i+1)*self.frameTot/self.nThreads))
        #     # threads.append(Thread(target=self.preLSTMtrain, args=(sliceBufferFrames, i, )))
        #     threads.append(Thread(target=self.preLSTMtrain, args=(self.bufferFrames, i, )))
        #     threads[i].start()
        #
        # time.sleep(1)
        #
        # # Wait for all threads to complete
        # for t in threads:
        #     t.join()

        while cnt < self.frameTot:
            for camera in self.cameras:
                ret, frame = camera.read()
                if ret and frame is not None:
                    frame = cv2.resize(frame,(IMAGE_WIDTH,IMAGE_HEIGHT))

                    bb = align.getLargestFaceBoundingBox(frame)
                    print(bb)
                    # select faces only viewed in the bigest bounding box and refuse a smaller ones
                    if bb is not None and bb.width() * bb.height() > IMAGE_WIDTH * IMAGE_HEIGHT / 20:
                        landmarks = align.findLandmarks(frame, bb)
                        alignedFace = align.align(
                            imgDim,
                            frame,
                            bb,
                            landmarks=landmarks,
                            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE
                        )

                        if alignedFace is not None and bb is not None:
                            cnt += 1
                            rep = net.forward(alignedFace).reshape(1, -1)
                            phash = str(imagehash.phash(Image.fromarray(alignedFace)))
                            images[phash] = Face(rep, self.numIdentity)

        self.images.update(images)
        print ("new images updated", len(self.images))

        self.trainLSTM()

        # if self.numIdentity > 7:
        #     for i in self.images.keys():
        #         if self.images[i].identity == self.numIdentity-5:
        #             del self.images[i]

        self.predictedUsers = []
        self.lengthPredictedUsers = 0
        self.bufferFrames.clear()
        self.isLockTraining = False
        self.FPS = 2

    # def preLSTMtrain(self, frames, numThread):
    #     cnt = 0
    #     images = {}
    #
    #     for frame in frames:
    #         cnt += 1
    #         # print ('Training face in ', numThread, ' iter ', cnt, np.size(frame))
    #
    #         bb = align.getLargestFaceBoundingBox(frame)
    #         print(bb)
    #         # select faces only viewed in the bigest bounding box and refuse a smaller ones
    #         if bb is not None and bb.width() * bb.height() > IMAGE_WIDTH * IMAGE_HEIGHT / 20:
    #             landmarks = align.findLandmarks(frame, bb)
    #             alignedFace = align.align(
    #                 imgDim,
    #                 frame,
    #                 bb,
    #                 landmarks=landmarks,
    #                 landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE
    #             )
    #
    #             if alignedFace is not None and bb is not None:
    #                 rep = net.forward(alignedFace).reshape(1, -1)
    #                 phash = str(imagehash.phash(Image.fromarray(alignedFace)))
    #                 images[phash] = Face(rep, self.numIdentity)
    #
    #     print('len images', len(images), len(self.images))
    #
    #     self.images.update(images)
    #     print ("new images updated", len(self.images))
    #
    def isBlurry(self, img, thresh = 100):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        variance = cv2.Laplacian(img, cv2.CV_64F).var()
        if variance < thresh:
            logging.warning('Image is blurry; Laplace variance %s', variance)
            return True

        return False

    def processFrame(self, frame, camId):
        frame = cv2.resize(frame,(IMAGE_WIDTH,IMAGE_HEIGHT))
        if self.isBlurry(frame):
            pass
        else:
            bb = align.getLargestFaceBoundingBox(frame)

            # select faces only viewed in the bigest bounding box and refuse a smaller ones
            if bb is not None and bb.width() * bb.height() > IMAGE_WIDTH * IMAGE_HEIGHT / 20:
                landmarks = align.findLandmarks(frame, bb)
                alignedFace = align.align(
                    imgDim,
                    frame,
                    bb,
                    landmarks=landmarks,
                    landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE
                )
                if alignedFace is not None and bb is not None:
                    rep = net.forward(alignedFace).reshape(1, -1)

                    if self.model.predict(rep) is None:
                        print ("no model exists we now need to train\n")
                        self.trainUnknownFace()
                        # self.preLSTMtrain(frame, 1)
                        # print ("calling now trraining part >>>>>>>....\n")
                        # self.trainLSTM()
                    else:
                        cls_id, conf = self.model.predict(rep)
                        print ("cls id {} and conf {}".format(cls_id, conf))

                    # calling the predict function from the model, if pre trained model exists the class id and confidence
                    # will be returned otherwise the frames will go into preLSTM code and will be trained upon

        print ("everything working perfectly till here\n")
